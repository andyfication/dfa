//Main Javascript function. Handles the program 
$(document).ready(() => {
  let init = setInterval(() => {
    init_app(init);
  }, 100);
});

//This will be used for the coursera API
courseraApi.callMethod({
  type: "GET_SESSION_CONFIGURATION",
  onSuccess: init_coursera
});


function init_coursera() {
  let init = setInterval(() => {
    init_app(init);
  }, 100);
}

// Initialise the App
function init_app(init) {
  // if on main container, load the app
  if (initInterval) {
    // Initialise network with new nodes and options
    network = new vis.Network(container, generateNewGraph(Math.random()), options);
    // Initialise Head Node
    initEntryNode();
    // Initialise Rest of the Nodes
    initNodes();
    // Scale initial network
    network.once('stabilized', function () {
      var scaleOption = {
        scale: 1.2
      };
      network.moveTo(scaleOption);
    })
    // Create custom copy of the vis network for computation 
    initialiseNetwork();
    // Initialise random string
    currentString = generateNewString();
    // Only make the graph interactive once positioned 
    let initGame = setInterval(() => {
      if (graphPositioned) {
        // these are all options in full.
        var options = {
          interaction: {
            dragNodes: false,
            dragView: true
          }
        }
        network.setOptions(options);
        // Validate the current network and constructing user taks array 
        initialiseValidation(currentString);
        // passing current nodes, tasks and the network to the player 
        player = new Player(nodes, tasks, network, currentString);
        // Init the player position at entry node 
        player.initStartingPosition();
        // Check which nodes we are clicking
        network.on('click', function (properties) {
          console.log('hello');
          var ids = properties.nodes;
          var clickedNode = nodes.get(ids);
          // Not clicking on a node check
          if (clickedNode.length != 0)
            // check player position and update network based on player tasks
            player.checkClickedNode(clickedNode);
        });
        // check player answer 'yes' at the end of the network 
        $('.answer')[0].addEventListener('click', (e) => {
          console.log(e.target.value);
          player.acceptStringAnswer(e);
        });
        // check player answer 'no' at the end of the network 
        $('.answer')[1].addEventListener('click', (e) => {
          console.log(e.target.value);
          player.rejectStringAnswer(e);
        });
        clearInterval(initGame);
        graphPositioned = false;
      }
    });

    //clear the interval for initApp. Only call it once after the intro animation
    clearInterval(init);

    // Event Handlers for node pointer 
    network.on("hoverNode", function (params) {
      network.canvas.body.container.style.cursor = 'pointer';
    });

    network.on("blurNode", function (params) {
      network.canvas.body.container.style.cursor = 'default';
    });

    // Event Handler for random string ----------------------------------------------------------
    $('#info-random-string-button').on('click', () => {
      // Hide Answer Buttons 
      $('.answer').css('display', 'none');
      // No longer suggesting to get a random string 
      $('#info-container-buttons').removeClass('highlightBorder');
      // No longer suggesting to get a random string 
      $('#info-container-text').removeClass('highlightBorder');
      // Set the right task
      currentTutorialCount = 7;
      tutorial[currentTutorialCount].set();
      // Initialise random string
      currentString = generateNewString();
      console.log(currentString);
      // Validate the current network and constructing user taks array 
      initialiseValidation(currentString);
      // Reset initial string classe name so that the player can use it 
      resetArrayClasses();
      // passing current nodes, tasks and the network to the player 
      player = new Player(nodes, tasks, network, currentString);
      // Init the player position at entry node 
      player.initStartingPosition();
    });

    // Event Handler for random graph ---------------------------------------------------------
    $('#info-random-graph-button').on('click', () => {
      // Hide Answer Buttons 
      $('.answer').css('display', 'none');
      // No longer suggesting to get a random string and grey out all sections except graph
      $('#info-container-buttons').removeClass('highlightBorder');
      $('#info-container-text').removeClass('highlightBorder');
      $('#info-container-buttons-transparent').css('display', 'block');
      $('#info-container-text-transparent').css('display', 'block');
      $('#info-container').addClass('non-active');
      // Set the right task
      currentTutorialCount = 6;
      tutorial[currentTutorialCount].set();

      // Initialise network with new nodes and options
      network = new vis.Network(container, generateNewGraph(Math.random()), options);
      // Initialise Head Node
      initEntryNode();
      // Initialise Rest of the Nodes
      initNodes();
      // Scale initial network
      network.once('stabilized', function () {
        var scaleOption = {
          scale: 1.2
        };
        network.moveTo(scaleOption);
      })
      // Create custom copy of the vis network for computation 
      initialiseNetwork();
      // Initialise random string
      currentString = generateNewString();
      // Reset initial string classe name so that the player can use it 
      resetArrayClasses();
      // Only make the graph interactive once positioned 
      let initGame = setInterval(() => {
        if (graphPositioned) {
          // these are all options in full.
          var options = {
            interaction: {
              dragNodes: false,
              dragView: true
            }
          }
          network.setOptions(options);
          // Validate the current network and constructing user taks array 
          initialiseValidation(currentString);
          // passing current nodes, tasks and the network to the player 
          player = new Player(nodes, tasks, network, currentString);
          // Init the player position at entry node 
          player.initStartingPosition();
          // Check which node we are clicking
          network.on('click', function (properties) {
            console.log('hello');
            var ids = properties.nodes;
            var clickedNode = nodes.get(ids);
            // Not clicking on a node check
            if (clickedNode.length != 0)
              // check player position and update network based on player tasks
              player.checkClickedNode(clickedNode);
          });
          // check player answer 'yes' at the end of the network 
          $('.answer')[0].addEventListener('click', (e) => {
            console.log(e.target.value);
            player.acceptStringAnswer(e);
          });
          // check player answer 'no' at the end of the network 
          $('.answer')[1].addEventListener('click', (e) => {
            console.log(e.target.value);
            player.rejectStringAnswer(e);
          });
          clearInterval(initGame);
          graphPositioned = false;
        }
      });
    });
  }
}