// Entry point;
// Update intro array with random 0 and 1
// Create intro network 
$(document).ready(() => {
  let listOfValues = $('.intro-array-element');
  introInterval = setInterval(() => {
    assignValueToList(listOfValues)
  }, 1500);
  generateIntroNetwork();
});

// Generates a random value 1 or 0
function generateOneOrZero() {
  let randomValue = Math.random();
  if (randomValue > 0.5) {
    randomValue = 1;
  } else
    randomValue = 0;
  return '<span class="intro-element">' + randomValue + '</span>';
}
// Assign the generated value to the array list
function assignValueToList(listOfValues) {
  console.log('Running')
  for (let index = 0; index < listOfValues.length; index++) {
    listOfValues[index].innerHTML = generateOneOrZero();
  }
}

// Create intro network graph 
function generateIntroNetwork() {
  let nodes = new vis.DataSet(
    [{
        id: 1,
        label: 'A',
        accepted: false
      },
      {
        id: 2,
        label: 'B',
        accepted: false
      },
      {
        id: 3,
        label: 'C',
        accepted: false
      }
    ]);

  let edges = new vis.DataSet([{

      from: 1,
      to: 2,
      label: ' 0 ',
      id: 1,
      val: 0,
      font: {
        size: 15,
        background: '#ffffff'
      }
    },
    {
      from: 2,
      to: 3,
      label: ' 1 ',
      id: 2,
      val: 1,
      font: {
        size: 15,
        background: '#ffffff'
      }
    },
    {
      from: 3,
      to: 1,
      label: ' 0 ',
      id: 3,
      val: 0,
      font: {
        size: 15,
        background: '#ffffff'
      }
    }
  ]);

  // Grab the container variable 
  let container = $('#intro_canvas')[0];

  // Create the Data Set
  let data = {
    nodes: nodes,
    edges: edges
  };

  // Set Data options 
  let options = {
    autoResize: true,
    height: '100%',
    width: '100%',
    interaction: {
      hover: true
    },
    nodes: {
      color: {
        background: '#fbd16a',
        border: '#eaaa3a',
        highlight: '#eaaa3a',
        hover: '#eaaa3a'
      }

    },
    edges: {
      arrows: {
        to: {
          enabled: true,
          scaleFactor: 0.8,
          type: 'arrow'
        }
      },
      color: {
        color: '#00FFFF',
        highlight: '#848484',
        hover: '#848484',
        inherit: 'from',
        opacity: 0.5
      },
      chosen: false
    },
    layout: {
      improvedLayout: true,
      hierarchical: {
        enabled: false
      }
    }
  };

  // Construct initial network and scale it
  var intro_network = new vis.Network(container, data, options);
  intro_network.once('stabilized', function () {
    var scaleOption = {
      scale: 2.2
    };
    intro_network.moveTo(scaleOption);
  })

  // Make pointer when Hovering a node
  intro_network.on("hoverNode", function (params) {
    intro_network.canvas.body.container.style.cursor = 'pointer';
  });
  // Make default pointer when Hovering a node
  intro_network.on("blurNode", function (params) {
    intro_network.canvas.body.container.style.cursor = 'default';
  });
  // Hide the initial popUp ('Move Me Around !')
  $('#intro_canvas').on('click', () => {
    $('#popup').fadeOut('1000');
  });



}