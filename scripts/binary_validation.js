// Initialise the current stream acceptance
let binaryStringAccepted = false;
// Initialise the custom Network data taken from Vis Network
let dfaFromNetwork = {};
// Initialise the sequence of tasks that the player needs to perform
let tasks = [];

// Initialise custom dataSet from Vis network (every time a new graph is generated)
function initialiseNetwork() {
  dfaFromNetwork = createDFAfromNetwork(nodes, edges);
}

// Initialise current graph and binary string validation. Records all the steps and whether the string is accepted
function initialiseValidation(string) {
  initValidation(string);
}

//recursive function that does the heavy lifting
function validateBinaryString(inputString, currentState, counter) {
  binaryStringAccepted = false;
  //display the dfa's 'logic' on the page
  printCurrentState("Current State is: " + currentState + ", Remaining Input Stream: " + inputString);
  //check if we still have inputs to process
  if (inputString.length > 0) {
    //get next state
    var nextState = getNextState(currentState, inputString[0]);
    // Add current task to the tasks object (each object has the current state and acceptance state)
    let newTask = {};
    newTask.currentLabel = currentState;
    newTask.nextLabel = nextState;
    newTask.accepted = dfaFromNetwork[nextState].isAccept;
    newTask.id = dfaFromNetwork[nextState].id;
    tasks[counter] = newTask;
    counter += 1;
    //recurse through the function with the first element of the string sliced off
    return validateBinaryString(inputString.slice(1), nextState, counter);
  } else if (dfaFromNetwork[currentState].isAccept == true) {
    //check if current state is an acceptance state
    binaryStringAccepted = true;
    return "String Accepted: " + binaryStringAccepted;
  } else {
    //current state is not an acceptance state so it must be rejected
    binaryStringAccepted = false;
    return "String Accepted: " + binaryStringAccepted;
  }
}

// Get the next state in the network 
function getNextState(currentState, binaryString) {
  // Check for valid input 
  if (dfaFromNetwork.vocabulary.includes(binaryString)) {
    // return next state
    return dfaFromNetwork[currentState][binaryString];
  } else {
    // return an error message
    return "Input not valid: " + binaryString;
  }
}

function printCurrentState(state) {
  // Only used for debugging
  //console.log(state); 
}



function initValidation(string) {
  // Grab the binary string from the input value
  var inputString = string;
  //
  // Run the valiadtion using the start state declared (input with no spaces, and begin state)
  var result = validateBinaryString(inputString.trim(), dfaFromNetwork.startState, 0);
  // Display the Current result
  console.log(result);
}

// Creating network to traverse from the vis.js network
function createDFAfromNetwork(nodes, edges) {
  // outer object which contains the inner objects of the network 
  let outerObj = {};
  // counter to visit all the edges (twice the amount of nodes)
  let edgeCounter = 1;
  // visit all nodes
  for (index = 1; index < nodes.length; index++) {

    // getting the node label information
    let tempString = nodes.get(index).label;
    // inner object initialisation
    let innerObj = {};
    // set the inner object name to the node label
    outerObj[tempString] = innerObj;

    // Add two properties to the inner Obj. Edge 0 goes to next node label and edge b goes to next next node label
    innerObj[0] = nodes.get(edges.get(edgeCounter).to).label;
    innerObj[1] = nodes.get(edges.get(edgeCounter + 1).to).label;

    // Add acceted label property to see if the node accepts the input
    innerObj.isAccept = nodes.get(index).accepted;
    // Add current node id property to keep track of where we are
    innerObj.id = nodes.get(index).id;
    // increment the counter by two to cover all the edges (2 each node)
    edgeCounter += 2;
  }
  // Getting label 'A', first node
  outerObj.startState = nodes.get(1).label;
  // The edge can be either a 1 or 0 
  outerObj.vocabulary = "01";
  return outerObj;
}