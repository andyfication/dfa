class Player {
  constructor(nodes, tasks, network, currentString) {
    this.network = network;
    // List of nodes to analyze
    this.nodes = nodes;
    // List of tasks
    this.tasks = tasks;
    // Current task to execute
    this.currentTask = 0;
    // Node id, starting from the first node 
    this.currentId = 1;
    // Current String we are playing with
    this.currentString = currentString;
    // List we are playing with
    this.listToPlay = $('.array-element');
  }

  // Initialise with task number one end highlighting the entry node
  initStartingPosition() {
    // Reset current info state to current node lable 
    $('#info-state').html(this.tasks[this.currentTask].currentLabel);
    // Reset Graph Interaction 
    var options = {
      interaction: {
        selectable: true,
        hover: true
      }
    }
    this.network.setOptions(options);
    console.log('called');
    this.currentTask = 0;
    this.currentId = 1;
    // Reset the graph colours
    for (let index = 1; index < this.nodes.length; index++) {
      if (this.nodes.get(index).accepted) {
        this.nodes.update({
          id: index,
          color: {
            background: 'rgb(40,40,40)',
            border: '#6fdc6f',
            highlight: {
              background: 'rgb(40,40,40)',
              border: '#6fdc6f'
            }
          }
        });
      } else {
        this.nodes.update({
          id: index,
          color: {
            background: 'rgb(40,40,40)',
            border: 'white',
            highlight: {
              background: 'rgb(40,40,40)',
              border: 'white'
            }
          }
        });
      }
    }
    this.nodes.update({
      id: this.currentId,
      color: {
        border: 'white',
        background: '#ff9900'
      }
    });
    this.initList();
  }
  // Function that checks on button 'yes'
  acceptStringAnswer(e) {
    console.log(e.target.value == this.tasks[this.currentTask].accepted.toString());
    if (e.target.value == this.tasks[this.currentTask].accepted.toString()) {
      $('#suggestion').html('Well Done, The current String is Accepted');
      $('.answer').css('display', 'none');
      $('#info-container-text').removeClass('highlightBorder');
      $('#info-container-buttons').addClass('highlightBorder');
    } else
      $('#suggestion').html('Not Really, Can the String be accepted ?');
  }
  // Function that checks on button 'no'
  rejectStringAnswer(e) {
    if (e.target.value == this.tasks[this.currentTask].accepted.toString()) {
      $('#suggestion').html('Well Done, The current String is Rejected');
      $('.answer').css('display', 'none');
      $('#info-container-text').removeClass('highlightBorder');
      $('#info-container-buttons').addClass('highlightBorder');
    } else
      $('#suggestion').html('Not Really, Can the String be accepted ?');
  }

  // Checks player current positions and moves
  checkClickedNode(node) {
    // If we reach the end of the string show button to decide if string is accepted
    if (this.currentTask == this.tasks.length - 1) {
      if (node[0].label == this.tasks[this.currentTask].nextLabel) {
        $('#suggestion').html('Can the Automaton accept this String ?');
        $('#info-state').html(this.tasks[this.currentTask].nextLabel);
        $('.answer').css('display', 'inline-block');
        $('#info-container-text').addClass('highlightBorder');
        // All visited characters in the list
        this.endOfList();
        // clear the other nodes design based on if they are labled accepted or not 
        for (let index = 1; index < this.nodes.length; index++) {
          if (this.nodes.get(index).accepted) {
            this.nodes.update({
              id: index,
              borderWidth: 2,
              color: {
                background: 'rgb(40,40,40)',
                highlight: {
                  border: '#6fdc6f',
                  background: 'rgb(40,40,40)'
                }
              }
            });
          } else {
            this.nodes.update({
              id: index,
              borderWidth: 2,
              color: {
                background: 'rgb(40,40,40)',
                highlight: {
                  background: 'rgb(40,40,40)'

                }
              }
            });
          }
        }
        // clear first node design 
        this.nodes.update({
          id: this.currentId,
          color: {
            border: 'white',
            background: 'rgb(40,40,40)'
          }
        });
        // update current node design 
        this.nodes.update({
          id: this.tasks[this.currentTask].id,
          color: {
            background: '#ff9900',
            border: "rgb(40,40,40)",
            highlight: {
              background: '#ff9900',
              border: "rgb(40,40,40)"
            }
          }
        });
        // We have reached the end, disable graph interaction
        var options = {
          interaction: {
            selectable: false,
            hover: false
          }
        }
        this.network.setOptions(options);
        // Reached last task but picking the wrong node
      } else if (node[0].label !== this.tasks[this.currentTask].nextLabel) {
        $('#suggestion').html('Not Really, Check your current State and the String Manager');
        $('#info-state').html(this.tasks[this.currentTask].currentlabel);
        // update current node design to red as this is not the correct node
        console.log(node[0].id + 'red')
        this.nodes.update({
          id: node[0].id,
          color: {
            highlight: {
              background: 'red'
            }
          }
        });
      }
    }
    // If we pick the correct node label then we update
    else if (node[0].label == this.tasks[this.currentTask].nextLabel) {
      // $('#suggestion').html('Nice Move: </br> Your current state is :' + this.tasks[this.currentTask].nextLabel);
      $('#suggestion').html('Well Done, Now make a Transition to the next Node');
      $('#info-state').html(this.tasks[this.currentTask].nextLabel);
      // clear the other nodes design
      for (let index = 1; index < this.nodes.length; index++) {
        if (this.nodes.get(index).accepted) {
          this.nodes.update({
            id: index,
            borderWidth: 2,
            color: {
              background: 'rgb(40,40,40)',
              highlight: {
                border: '#6fdc6f',
                background: 'rgb(40,40,40)'
              }
            }
          });
        } else {
          this.nodes.update({
            id: index,
            borderWidth: 2,
            color: {
              background: 'rgb(40,40,40)',
              highlight: {
                background: 'rgb(40,40,40)'

              }
            }
          });
        }
      }
      // clear first node design 
      this.nodes.update({
        id: this.currentId,
        color: {
          border: 'white',
          background: 'rgb(40,40,40)'
        }
      });
      // update current node design 
      this.nodes.update({
        id: this.tasks[this.currentTask].id,
        color: {
          background: '#ff9900',
          highlight: {
            background: '#ff9900'

          }

        }
      });
      // we move to the next task 
      this.currentTask = this.currentTask < tasks.length - 1 ? this.currentTask += 1 : this.currentTask;
      // Update string character to show the current highlighted string character  
      this.updateList();
    } // If wrong node is selected 
    else if (node[0].label !== this.tasks[this.currentTask].nextLabel) {
      // $('#suggestion').html('Wrong Node: </br> Your current state is :' + this.tasks[this.currentTask].currentLabel);
      $('#suggestion').html('Not Really, Check your current State and the String Manager');
      $('#info-state').html(this.tasks[this.currentTask].currentlabel);
      console.log(this.currentTask);
      // update current node design to red as this is not the correct node
      console.log(node[0].id + 'red')
      if (node[0].accepted) {
        this.nodes.update({
          id: node[0].id,
          color: {
            border: '#6fdc6f',
            highlight: {
              background: 'red',
              border: '#6fdc6f'
            }
          }
        });
      } else {
        this.nodes.update({
          id: node[0].id,
          color: {
            border: 'white',
            highlight: {
              background: 'red',
              border: 'white'
            }
          }
        });
      }
    }
  }
  // Initialise the current string array with the right colours interaction 
  initList() {
    // Remove Class from the array items 
    for (let index = 0; index < this.listToPlay.length; index++) {
      $(this.listToPlay[index]).removeClass('array_done');
    }
    // Add Class from the array items
    for (let index = 0; index < this.listToPlay.length; index++) {
      $(this.listToPlay[index]).addClass('array-element');
    }
    // Assign list to array 
    for (let index = 0; index < this.listToPlay.length; index++) {
      $(this.listToPlay[index]).html('<span class="element">' + this.currentString[index] + '</span>');
    }
    // Hide all elements except the first one on te list
    for (let index = 1; index < this.listToPlay.length; index++) {
      $(this.listToPlay[index]).addClass('hide_element');
      $(this.listToPlay[index]).removeClass('array-element');
    }

  }
  // Update the list colours
  updateList() {
    // reset colours 
    for (let index = 0; index < this.listToPlay.length; index++) {
      $(this.listToPlay[index]).removeClass('array-element');
      $(this.listToPlay[index]).addClass('hide_element');
    }
    // Add highlight on current task 
    $(this.listToPlay[this.currentTask]).removeClass('hide_element');
    $(this.listToPlay[this.currentTask]).addClass('array-element');
    // Add visited on previous task 
    $(this.listToPlay[this.currentTask - 1]).removeClass('hide_element');
    $(this.listToPlay[this.currentTask - 1]).removeClass('array-element');
    $(this.listToPlay[this.currentTask - 1]).addClass('array_done');
  }
  // All visited at the end of the string 
  endOfList() {
    for (let index = 0; index < this.listToPlay.length; index++) {
      $(this.listToPlay[index]).removeClass('array-element');
      $(this.listToPlay[index]).removeClass('hide_element');
      $(this.listToPlay[index]).addClass('array_done');
    }
  }
}