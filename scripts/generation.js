// This function generates a binary string with eiter a 1 or a 0
function generateNewString() {
  let stringLen = 6;
  let newString = '';
  for (let counter = 0; counter < stringLen; counter++) {
    let randVal = Math.random();
    if (randVal > 0.5) {
      newString += '1';
    } else
      newString += '0';
  }
  return newString;
}
// This function generates a new random Graph (between 4 and 6 nodes and twice the edges of the node number)
function generateNewGraph(probability) {
  // nodes variable
  nodes = new vis.DataSet();
  // edges variable
  edges = new vis.DataSet();
  // number of nodes based on probability 
  let numberOfNodes;
  // number of edges
  let numberOfEdges;
  // Edges id
  let edgeId = 1;
  // number of nodes aray
  let numberOfNodesArray = [];
  // Previous Random Value
  let prevRandom;
  // Accepted state 
  let acc;
  // Only a maximum of a third of the number of nodes can have accept state. Minimum none.
  let maxFourAccNodes = false;
  let maxFiveAccNodes = false;
  let maxSixAccNodes = false;
  let maxSixAccNodesCounter = 0;
  // The graph can have either 4,5 or 6 nodes based on probability
  if (probability <= 0.3) {
    numberOfNodes = 4;
  } else if (probability > 0.3 && probability <= 0.6) {
    numberOfNodes = 5;
  } else {
    numberOfNodes = 6;
  }
  // Fill the array to keep track of the nodes id
  for (index = 1; index <= numberOfNodes; index++) {
    numberOfNodesArray.push(index);
  }
  // Construct the nodes network 
  for (index = 1; index <= numberOfNodes; index++) {
    let letters = ['A', 'B', 'C', 'D', 'E', 'F'];
    // Random value to determine if the node is accepted or rejected according to the number of nodes 
    if (numberOfNodes == 4) {
      if (!maxFourAccNodes) {
        acc = Math.random() >= 0.5 ? true : false;
        if (acc == true) {
          maxFourAccNodes = true;
        }
      } else {
        acc = false;
      }
      // Have at least one node with accept state
      if (index == numberOfNodes && maxFourAccNodes == false) {
        acc = true;
      }
    } else if (numberOfNodes == 5) {
      if (!maxFiveAccNodes) {
        acc = Math.random() >= 0.5 ? true : false;
        if (acc == true) {
          maxFiveAccNodes = true;
        }
      } else {
        acc = false;
      }
      // Have at least one node with accept state
      if (index == numberOfNodes && maxFiveAccNodes == false) {
        acc = true;
      }
    } else if (numberOfNodes == 6) {
      if (!maxSixAccNodes) {
        acc = Math.random() >= 0.5 ? true : false;
        if (acc == true) {
          maxSixAccNodesCounter += 1;
          if (maxSixAccNodesCounter == 2) {
            maxSixAccNodes = true;
          }
        }
      } else {
        acc = false;
      }
      // Have at least one node with accept state
      if (index == numberOfNodes && maxSixAccNodes == false) {
        acc = true;
      }
    }
    let temp_id = 0;
    let temp_label = '';
    let temp_accepted = false;
    if (index == 0) {
      temp_id = -1;
      temp_label = 'Entry Point';
      temp_accepted = false;
    } else {
      temp_id = index;
      temp_label = letters[index - 1];
      temp_accepted = acc;
    }
    nodes.add({
      id: temp_id,
      label: temp_label,
      accepted: temp_accepted
    });
  }
  numberOfEdges = numberOfNodes;

  // Construct the edges structure
  for (let index = 0; index <= numberOfEdges; index++) {
    let temp_from;
    let temp_to;
    let temp_id;
    let temp_val;
    let temp_label;
    let tempFont = {}

    if (index == 0) {
      temp_from = -1;
      temp_to = 1;
      temp_id = index;
      temp_val = -1;

      edges.add({
        from: temp_from,
        to: temp_to,
        id: temp_id,
        val: temp_val
      });

    } else {
      // There are two edges for every node 
      for (let edgeAmount = 0; edgeAmount < 2; edgeAmount++) {
        temp_from = index;
        temp_current_Random = Math.floor(Math.random() * numberOfNodesArray.length);
        // Make sure that no more than 1 edge with the same value goes to a specific node 
        if (temp_current_Random != prevRandom)
          temp_to = numberOfNodesArray[temp_current_Random];
        else {
          while (temp_current_Random == prevRandom) {
            temp_current_Random = Math.floor(Math.random() * numberOfNodesArray.length);
          }
          temp_to = numberOfNodesArray[temp_current_Random];
        }
        temp_label = edgeAmount.toString();
        temp_id = edgeId;
        temp_val = edgeAmount;
        edgeId += 1;
        tempFont.size = 15;
        tempFont.background = '#ffffff';
        prevRandom = temp_current_Random;

        edges.add({
          from: temp_from,
          to: temp_to,
          label: temp_label,
          id: temp_id,
          val: temp_val,
          font: tempFont
        });
      }
    }
  }
  // Create the Data Set
  data = {
    nodes: nodes,
    edges: edges
  };

  return data;
}